package sample1;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import net.osmand.core.android.CoreResourcesFromAndroidAssets;
import net.osmand.core.jni.AtlasMapRendererConfiguration;
import net.osmand.core.jni.BinaryMapDataProvider;
import net.osmand.core.jni.BinaryMapPrimitivesProvider;
import net.osmand.core.jni.BinaryMapRasterLayerProvider;
import net.osmand.core.jni.BinaryMapRasterLayerProvider_Software;
import net.osmand.core.jni.BinaryMapStaticSymbolsProvider;
import net.osmand.core.jni.IMapRenderer;
import net.osmand.core.jni.IMapStylesCollection;
import net.osmand.core.jni.MapPresentationEnvironment;
import net.osmand.core.jni.MapRendererClass;
import net.osmand.core.jni.MapStylesCollection;
import net.osmand.core.jni.ObfsCollection;
import net.osmand.core.jni.OsmAndCore;
import net.osmand.core.jni.PointI;
import net.osmand.core.jni.Primitiviser;
import net.osmand.core.jni.ResolvedMapStyle;
import net.osmand.core.samples.android.sample1.R;

public class MainActivity extends Activity {

    static {

        try {
            System.loadLibrary("gnustl_shared");
        }
        catch( UnsatisfiedLinkError e ) {
            System.err.println("Failed to load 'gnustl_shared':" + e);
            System.exit(0);
        }

        try {
            System.loadLibrary("Qt5Core");
        }
        catch( UnsatisfiedLinkError e ) {
            System.err.println("Failed to load 'Qt5Core':" + e);
            System.exit(0);
        }

        try {
            System.loadLibrary("Qt5Network");
        }
        catch( UnsatisfiedLinkError e ) {
            System.err.println("Failed to load 'Qt5Network':" + e);
            System.exit(0);
        }

        try {
            System.loadLibrary("Qt5Sql");
        }
        catch( UnsatisfiedLinkError e ) {
            System.err.println("Failed to load 'Qt5Sql':" + e);
            System.exit(0);
        }

        try {
            System.loadLibrary("OsmAndCoreWithJNI");
        }
        catch( UnsatisfiedLinkError e ) {
            System.err.println("Failed to load 'OsmAndCoreWithJNI':" + e);
            System.exit(0);
        }
    }

    private static final String TAG = "OsmAndCoreSample";

    private IMapStylesCollection mMapStylesCollection;
    private ResolvedMapStyle mMapStyle;
    private ObfsCollection mObfsCollection;
    private MapPresentationEnvironment mMapPresentationEnvironment;
    private Primitiviser mPrimitiviser;
    private BinaryMapDataProvider mBinaryMapDataProvider;
    private BinaryMapPrimitivesProvider mBinaryMapPrimitivesProvider;
    private BinaryMapStaticSymbolsProvider mBinaryMapStaticSymbolsProvider;
    private BinaryMapRasterLayerProvider mBinaryMapRasterLayerProvider;
    private IMapRenderer mMapRenderer;
    //private QIODeviceLogSink mFileLogSink;

    private GLSurfaceView mGLSurfaceView;

    private float mZoom = 10;
    private Renderer mRenderer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get device display density factor
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final float displayDensityFactor = displayMetrics.densityDpi / 160.0f;
        final int referenceTileSize = (int) (256 * displayDensityFactor);
        final int rasterTileSize = Integer.highestOneBit(referenceTileSize - 1) * 2;

        Log.i(TAG, "DisplayDensityFactor = " + displayDensityFactor);
        Log.i(TAG, "ReferenceTileSize = " + referenceTileSize);
        Log.i(TAG, "RasterTileSize = " + rasterTileSize);

        final ActivityManager activityManager =
                (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo configurationInfo =
                activityManager.getDeviceConfigurationInfo();
        final boolean supportsEs2 = configurationInfo.reqGlEsVersion >= 0x20000;

        Log.i(TAG, "Supports OpenGL ES 2:" + supportsEs2);

        Log.i(TAG, "Initializing core...");
        final CoreResourcesFromAndroidAssets coreResources
                = CoreResourcesFromAndroidAssets.loadFromCurrentApplication(this);
        OsmAndCore.InitializeCore(coreResources.instantiateProxy());

        //mFileLogSink = QIODeviceLogSink.createFileLogSink(
        //        Environment.getExternalStorageDirectory() + "/osmand/osmandcore.log");
        //Logger.get().addLogSink(mFileLogSink);

        Log.i(TAG, "Going to resolve default embedded style...");
        mMapStylesCollection = new MapStylesCollection();
        mMapStyle = mMapStylesCollection.getResolvedStyleByName("default");
        if (mMapStyle == null) {
            Log.e(TAG, "Failed to resolve style 'default'");
            System.exit(0);
        }

        Log.i(TAG, "Going to prepare OBFs collection");
        mObfsCollection = new ObfsCollection();
        Log.i(TAG, "Will load OBFs from " + Environment.getExternalStorageDirectory() + "/osmand");
        mObfsCollection.addDirectory(Environment.getExternalStorageDirectory() + "/osmand", false);

        Log.i(TAG, "Going to prepare all resources for renderer");
        mMapPresentationEnvironment = new MapPresentationEnvironment(
                mMapStyle,
                displayDensityFactor,
                "en"); //TODO: here should be current locale
        //mapPresentationEnvironment->setSettings(configuration.styleSettings);
        mPrimitiviser = new Primitiviser(mMapPresentationEnvironment);
        mBinaryMapDataProvider = new BinaryMapDataProvider(mObfsCollection);
        mBinaryMapPrimitivesProvider = new BinaryMapPrimitivesProvider(
                mBinaryMapDataProvider, mPrimitiviser, rasterTileSize);
        mBinaryMapStaticSymbolsProvider = new BinaryMapStaticSymbolsProvider(
                mBinaryMapPrimitivesProvider, rasterTileSize);
        mBinaryMapRasterLayerProvider = new BinaryMapRasterLayerProvider_Software(
                mBinaryMapPrimitivesProvider);

        Log.i(TAG, "Going to create renderer");
        mMapRenderer = OsmAndCore.createMapRenderer(MapRendererClass.AtlasMapRenderer_OpenGLES2);
        if (mMapRenderer == null) {
            Log.e(TAG, "Failed to create map renderer 'AtlasMapRenderer_OpenGLES2'");
            System.exit(0);
        }
        final AtlasMapRendererConfiguration atlasRendererConfiguration
                = AtlasMapRendererConfiguration.Casts.upcastFrom(mMapRenderer.getConfiguration());
        atlasRendererConfiguration.setReferenceTileSizeOnScreenInPixels(referenceTileSize);
        mMapRenderer.setConfiguration(
                AtlasMapRendererConfiguration.Casts
                        .downcastTo_MapRendererConfiguration(atlasRendererConfiguration));
        mMapRenderer.addSymbolsProvider(mBinaryMapStaticSymbolsProvider);
        mMapRenderer.setAzimuth(0.0f);
        mMapRenderer.setElevationAngle(35.0f);
        mMapRenderer.setTarget(new PointI(
                1102430866,
                704978678));
        mMapRenderer.setZoom(mZoom);
        mMapRenderer.setMapLayerProvider(0, mBinaryMapRasterLayerProvider);

        mGLSurfaceView = (GLSurfaceView) findViewById(R.id.glSurfaceView);
        mGLSurfaceView.setPreserveEGLContextOnPause(true);
        mGLSurfaceView.setEGLContextClientVersion(2);
        mGLSurfaceView.setEGLConfigChooser(true);
        //mGLSurfaceView.setEGLContextFactory(new EGLContextFactory(mGLSurfaceView, mMapRenderer));
        mRenderer = new Renderer(mMapRenderer);
        mGLSurfaceView.setRenderer(mRenderer);
        mGLSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

        final Button zoomInButton = (Button) findViewById(R.id.zoom_in_btn_view);
        zoomInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMapRenderer.setZoom(++mZoom);
            }
        });

        final Button zoomOutButton = (Button) findViewById(R.id.zoom_out_btn_view);
        zoomOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMapRenderer.setZoom(--mZoom);
            }
        });
    }

    public void onStart(View view) {
        mRenderer.onStart();
    }

    public void onStop(View view) {
        mRenderer.onStop();
    }

    public void onSnapshot(View view) {
        // TODO :
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGLSurfaceView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGLSurfaceView.onResume();
    }

    @Override
    protected void onDestroy() {
        if (mMapStylesCollection != null) {
            mMapStylesCollection.delete();
            mMapStylesCollection = null;
        }

        if (mMapStyle != null) {
            mMapStyle.delete();
            mMapStyle = null;
        }

        if (mObfsCollection != null) {
            mObfsCollection.delete();
            mObfsCollection = null;
        }

        if (mMapPresentationEnvironment != null) {
            mMapPresentationEnvironment.delete();
            mMapPresentationEnvironment = null;
        }

        if (mPrimitiviser != null) {
            mPrimitiviser.delete();
            mPrimitiviser = null;
        }

        if (mBinaryMapDataProvider != null) {
            mBinaryMapDataProvider.delete();
            mBinaryMapDataProvider = null;
        }

        if (mBinaryMapPrimitivesProvider != null) {
            mBinaryMapPrimitivesProvider.delete();
            mBinaryMapPrimitivesProvider = null;
        }

        if (mBinaryMapStaticSymbolsProvider != null) {
            mBinaryMapStaticSymbolsProvider.delete();
            mBinaryMapStaticSymbolsProvider = null;
        }

        if (mBinaryMapRasterLayerProvider != null) {
            mBinaryMapRasterLayerProvider.delete();
            mBinaryMapRasterLayerProvider = null;
        }

        if (mMapRenderer != null) {
            mMapRenderer.delete();
            mMapRenderer = null;
        }

        OsmAndCore.ReleaseCore();

        super.onDestroy();
    }
}

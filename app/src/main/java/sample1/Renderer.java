package sample1;

import android.opengl.GLSurfaceView;
import android.util.Log;

import net.osmand.core.jni.AreaI;
import net.osmand.core.jni.IMapRenderer;
import net.osmand.core.jni.PointI;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 26.10.14
 * Time: 23:09
 */
public class Renderer implements GLSurfaceView.Renderer {

    private static final String TAG = "Renderer";

    private final IMapRenderer mMapRenderer;

    private int mGLWidth;
    private int mGLHeight;
    private boolean doRecord;
    //private Worker mWorker = new Worker();

    public Renderer(final IMapRenderer mapRenderer) {
        mMapRenderer = mapRenderer;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Log.i(TAG, "onSurfaceCreated");
        if (!mMapRenderer.isRenderingInitialized()) {
            return;
        }
        mMapRenderer.releaseRendering();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Log.i(TAG, "onSurfaceChanged:" + width + "x" + height);

        mGLWidth = width;
        mGLHeight = height;

        //mWorker.setGLDimensions(width, height);

        mMapRenderer.setViewport(new AreaI(0, 0, height, width));
        mMapRenderer.setWindowSize(new PointI(width, height));

        if (mMapRenderer.isRenderingInitialized()) {
            return;
        }
        if (mMapRenderer.initializeRendering()) {
            return;
        }
        Log.e(TAG, "Failed to initialize rendering");
    }

    @Override
    public void onDrawFrame(final GL10 gl) {
        //Log.i(TAG, "onDrawFrame");

        mMapRenderer.update();

        if (mMapRenderer.prepareFrame()) {
            mMapRenderer.renderFrame();
        }
    }

    public void onStart() {
        doRecord = true;
    }

    public void onStop() {
        doRecord = false;
    }
}
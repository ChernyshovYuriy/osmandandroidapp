package sample1;

import android.util.Log;

import net.osmand.core.jni.IMapRenderer;
import net.osmand.core.jni.MapRendererSetupOptions;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 26.10.14
 * Time: 22:55
 */
public class GpuWorkerThreadPrologue extends MapRendererSetupOptions.IGpuWorkerThreadPrologue {

    private static final String TAG = "GpuWorkerThreadPrologue";

    public GpuWorkerThreadPrologue(EGL10 egl, EGLDisplay eglDisplay, EGLContext context,
                                   EGLSurface surface) {
        mEGL10 = egl;
        mEGLDisplay = eglDisplay;
        mEGLContext = context;
        mEGLSurface = surface;
    }

    private final EGL10 mEGL10;
    private final EGLDisplay mEGLDisplay;
    private EGLContext mEGLContext;
    private EGLSurface mEGLSurface;

    @Override
    public void method(IMapRenderer mapRenderer) {
        try {
            if (!mEGL10.eglMakeCurrent(mEGLDisplay, mEGLSurface, mEGLSurface, mEGLContext))
                Log.e(TAG, "Failed to set GPU worker context active: " + mEGL10.eglGetError());
        } catch (Exception e) {
            Log.e(TAG, "Failed to set GPU worker context active", e);
        }
    }
}
package sample1;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.IntBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 23.10.14
 * Time: 20:08
 */
public class Worker {

    private static final String LOG_TAG = Worker.class.getSimpleName();

    private static final String BOUNDARY_START = "--myboundary\r\nContent-type: image/jpg\r\nContent-Length: ";
    private static final String BOUNDARY_END = "\r\n\r\n";

    private FileOutputStream mFileOutputStream;

    private ByteArrayOutputStream jpegByteArrayOutputStream;
    private BufferedOutputStream bufferedOutputStream;
    private int mGLWidth;
    private int mGLHeight;

    public Worker() {
        jpegByteArrayOutputStream = new ByteArrayOutputStream();
        final File mjpegFile = new File(Environment.getExternalStorageDirectory() + "/video.mjpeg");

        try {
            mFileOutputStream = new FileOutputStream(mjpegFile);
        } catch (FileNotFoundException e) {
            Log.e(LOG_TAG, "FileNotFoundException:" + e.getMessage());
        }

        bufferedOutputStream = new BufferedOutputStream(mFileOutputStream);
    }

    public void onStop() {
        try {
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
        } catch (IOException e) {
            Log.e(LOG_TAG, "IOException:" + e.getMessage());
        }
    }

    public void setGLDimensions(final int width, final int height) {
        Log.d(LOG_TAG, "GL dim:" + width + "x" + height);

        mGLWidth = width;
        mGLHeight = height;
    }

    public void setGL10(GL10 value) {
        Log.d(LOG_TAG, "GL:" + value);
        if (value == null) {
            return;
        }

        final Bitmap bitmap = createBitmapFromGLSurface(value);

        try {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, jpegByteArrayOutputStream);

            byte[] jpegByteArray = jpegByteArrayOutputStream.toByteArray();

            byte[] boundaryBytes =
                    (BOUNDARY_START + jpegByteArray.length + BOUNDARY_END).getBytes();
            bufferedOutputStream.write(boundaryBytes);
            bufferedOutputStream.write(jpegByteArray);
            bufferedOutputStream.flush();
        } catch (IOException e) {
            Log.e(LOG_TAG, "IOException:" + e.getMessage());
        }
    }

    private Bitmap createBitmapFromGLSurface(GL10 gl) {
        final int b[] = new int[mGLWidth * mGLHeight];
        final IntBuffer ib = IntBuffer.wrap(b);
        ib.position(0);
        gl.glReadPixels(0, 0, mGLWidth, mGLHeight, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, ib);

        // The bytes within the ints are in the wrong order for android, but convert into a
        // bitmap anyway. They're also bottom-to-top rather than top-to-bottom. We'll fix
        // this up soon using some fast API calls.
        final Bitmap glbitmap = Bitmap.createBitmap(b, mGLWidth, mGLHeight, Bitmap.Config.ARGB_4444);

        // To swap the color channels, we'll use a ColorMatrix/ColorMatrixFilter. From the Android docs:
        //
        // This is a 5x4 matrix: [ a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t ]
        // When applied to a color [r, g, b, a] the resulting color is computed as (after clamping):
        //
        // R' = a*R + b*G + c*B + d*A + e;
        // G' = f*R + g*G + h*B + i*A + j;
        // B' = k*R + l*G + m*B + n*A + o;
        // A' = p*R + q*G + r*B + s*A + t;
        //
        // We want to swap R and B, so the coefficients will be:
        // R' = B => 0,0,1,0,0
        // G' = G => 0,1,0,0,0
        // B' = R => 1,0,0,0,0
        // A' = A => 0,0,0,1,0

        final float[] cmVals = {0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0};

        final Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(new ColorMatrix(cmVals))); // our R<->B swapping paint

        final Bitmap bitmap = Bitmap.createBitmap(mGLWidth, mGLHeight, Bitmap.Config.ARGB_4444); // the bitmap we're going to draw onto
        final Canvas canvas = new Canvas(bitmap); // we draw to the bitmap through a canvas
        canvas.drawBitmap(glbitmap, 0, 0, paint); // draw the opengl bitmap onto the canvas, using the color swapping paint

        // the image is still upside-down, so vertically flip it
        final Matrix matrix = new Matrix();
        matrix.preScale(1.0f, -1.0f); // scaling: x = x, y = -y, i.e. vertically flip
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true); // new bitmap, using the flipping matrix
    }
}

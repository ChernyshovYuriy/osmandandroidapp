package sample1;

import android.util.Log;

import net.osmand.core.jni.IMapRenderer;
import net.osmand.core.jni.MapRendererSetupOptions;

import javax.microedition.khronos.egl.EGL10;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 26.10.14
 * Time: 22:56
 */
public class GpuWorkerThreadEpilogue extends MapRendererSetupOptions.IGpuWorkerThreadEpilogue {

    private static final String TAG = "GpuWorkerThreadEpilogue";

    public GpuWorkerThreadEpilogue(EGL10 egl) {
        mEGL10 = egl;
    }

    private final EGL10 mEGL10;

    @Override
    public void method(IMapRenderer mapRenderer) {
        try {
            if (!mEGL10.eglWaitGL())
                Log.e(TAG, "Failed to wait for GPU worker context: " + mEGL10.eglGetError());
        } catch (Exception e) {
            Log.e(TAG, "Failed to wait for GPU worker context", e);
        }
    }
}
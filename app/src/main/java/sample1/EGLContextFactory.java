package sample1;

import android.opengl.EGL14;
import android.opengl.GLSurfaceView;
import android.util.Log;

import net.osmand.core.jni.IMapRenderer;
import net.osmand.core.jni.MapRendererSetupOptions;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 26.10.14
 * Time: 22:51
 */
public class EGLContextFactory implements GLSurfaceView.EGLContextFactory {

    private static final String TAG = "EGLContextFactory";

    private EGLContext mGPUWorkerContext;
    private EGLSurface mGPUWorkerFakeSurface;
    private final GLSurfaceView mGLSurfaceView;
    private final IMapRenderer mMapRenderer;

    public EGLContextFactory(final GLSurfaceView glSurfaceView, final IMapRenderer mapRenderer) {
        mGLSurfaceView = glSurfaceView;
        mMapRenderer = mapRenderer;
    }

    public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig) {

        final String eglExtensions = egl.eglQueryString(display, EGL10.EGL_EXTENSIONS);
        Log.i(TAG, "EGL extensions: " + eglExtensions);
        final String eglVersion = egl.eglQueryString(display, EGL10.EGL_VERSION);
        Log.i(TAG, "EGL version: " + eglVersion);

        Log.i(TAG, "Creating main context...");
        // Configure context for OpenGL ES 2.0.
        final int[] contextAttribList = {
                EGL14.EGL_CONTEXT_CLIENT_VERSION, 2,
                EGL10.EGL_NONE };

        int[] version = new int[2];
        if (!egl.eglInitialize(display, version)) {
            throw new RuntimeException("unable to initialize EGL10");
        }

        EGLContext mainContext = null;
        try {
            mainContext = egl.eglCreateContext(display, eglConfig, EGL10.EGL_NO_CONTEXT,
                    contextAttribList);
        } catch (Exception e) {
            Log.e(TAG, "Failed to create main context", e);
        }
        if (mainContext == null || mainContext == EGL10.EGL_NO_CONTEXT) {
            Log.e(TAG, "Failed to create main context: " + egl.eglGetError());
            mainContext = null;
            System.exit(0);
        }
        Log.d(TAG, "OpenGLES main context = " + mainContext);

        Log.i(TAG, "Creating GPU worker context...");
        try {
            mGPUWorkerContext = egl.eglCreateContext(
                    display,
                    eglConfig,
                    mainContext,
                    contextAttribList);
        } catch (Exception e) {
            Log.e(TAG, "Failed to create GPU worker context", e);
        }
        if (mGPUWorkerContext == null || mGPUWorkerContext == EGL10.EGL_NO_CONTEXT)
        {
            Log.e(TAG, "Failed to create GPU worker context: " + egl.eglGetError());
            mGPUWorkerContext = null;
        }
        Log.d(TAG, "OpenGLES GPU worker context = " + mGPUWorkerContext);

        if (mGPUWorkerContext != null)
        {
            Log.i(TAG, "Creating GPU worker fake surface...");
            try {
                final int[] surfaceAttribList = {
                        EGL10.EGL_WIDTH, 1,
                        EGL10.EGL_HEIGHT, 1,
                        EGL10.EGL_NONE };
                mGPUWorkerFakeSurface = egl.eglCreatePbufferSurface(display, eglConfig,
                        surfaceAttribList);
            } catch (Exception e) {
                Log.e(TAG, "Failed to create GPU worker fake surface", e);
            }
            if (mGPUWorkerFakeSurface == null || mGPUWorkerFakeSurface == EGL10.EGL_NO_SURFACE)
            {
                Log.e(TAG, "Failed to create GPU worker fake surface: " + egl.eglGetError());
                mGPUWorkerFakeSurface = null;
            }
        }

        final MapRendererSetupOptions rendererSetupOptions = new MapRendererSetupOptions();
        if (mGPUWorkerContext != null && mGPUWorkerFakeSurface != null) {
            rendererSetupOptions.setGpuWorkerThreadEnabled(true);

            final GpuWorkerThreadPrologue mGPUWorkerThreadPrologue
                    = new GpuWorkerThreadPrologue(egl, display, mGPUWorkerContext,
                    mGPUWorkerFakeSurface);
            rendererSetupOptions.setGpuWorkerThreadPrologue(mGPUWorkerThreadPrologue.getBinding());

            final GpuWorkerThreadEpilogue mGPUWorkerThreadEpilogue = new GpuWorkerThreadEpilogue(egl);
            rendererSetupOptions.setGpuWorkerThreadEpilogue(mGPUWorkerThreadEpilogue.getBinding());
        } else {
            rendererSetupOptions.setGpuWorkerThreadEnabled(false);
        }

        final RenderRequestCallback mRenderRequestCallback = new RenderRequestCallback();
        rendererSetupOptions.setFrameUpdateRequestCallback(mRenderRequestCallback.getBinding());

        mMapRenderer.setup(rendererSetupOptions);

        return mainContext;
    }

    public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context) {
        egl.eglDestroyContext(display, context);

        if (mGPUWorkerContext != null) {
            egl.eglDestroyContext(display, mGPUWorkerContext);
            mGPUWorkerContext = null;
        }

        if (mGPUWorkerFakeSurface != null) {
            egl.eglDestroySurface(display, mGPUWorkerFakeSurface);
            mGPUWorkerFakeSurface = null;
        }
    }

    private class RenderRequestCallback extends MapRendererSetupOptions.IFrameUpdateRequestCallback {

        @Override
        public void method(IMapRenderer mapRenderer) {

            mGLSurfaceView.requestRender();
        }
    }
}
